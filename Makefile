##
## Makefile for  in /Users/grandr_r/Myre_Cpp
##
## Made by grandr_r
## Login   <grandr_r@padawan.ru>
##
## Started on  Thu Feb 22 10:02:56 2018 grandr_r
## Last update Thu Feb 22 10:15:00 2018 grandr_r
##

SRC=		src/main.cpp

OBJ=		$(SRC:.cpp=.o)

CXXFLAGS +=	-Wall -Wextra -Werror -std=c++1z

CXX=		~/x-tools/x86_64-uefi-elf/bin/x86_64-uefi-elf-g++

NAME_ELF=	Myre.elf

NAME_EFI=	efi_fs/Myre.efi

all:		$(NAME_EFI)

$(NAME_EFI):	$(OBJ)
		$(CXX) -o $(NAME_ELF) $(OBJ)
		./tools/elf2efi $(NAME_ELF) $(NAME_EFI)

clean:
		rm -f $(OBJ) *~ include/*~ src/*~

fclean:		clean
		rm -f $(NAME_ELF) $(NAME_EFI)

re:		fclean all
